package extra;

import java.util.*;

public class ScoreStatFormulaGenerator{

    public static void main(String []args){
        Map<String, Integer> projectFix = new HashMap<>();
        Map<String, Integer> projectReg = new HashMap<>();

        projectFix.put("tomcat", 3984);
        projectReg.put("tomcat", 56867);

        projectFix.put("guava", 486);
        projectReg.put("guava", 5996);

        projectFix.put("mockito", 807);
        projectReg.put("mockito", 4561);

        projectFix.put("commons-lang", 407);
        projectReg.put("commons-lang", 5512);

        projectFix.put("hadoop", 3097);
        projectReg.put("hadoop", 18338);

        projectFix.put("selenium", 545);
        projectReg.put("selenium", 23005);

        projectFix.put("elasticsearch", 8332);
        projectReg.put("elasticsearch", 36643);

        projectFix.put("spring-framework", 1849);
        projectReg.put("spring-framework", 16173);

        for(int i=-4; i<=4; i++){
            for(Map.Entry<String, Integer> entry: projectFix.entrySet()){
                System.out.println("=COUNTIF(\'" + entry.getKey() + "\'!A2:A" + (entry.getValue()+1) + ",\"="+ i + "\")/\'" + entry.getKey() + "\'!G6*100");
            }
            System.out.println();

            for(Map.Entry<String, Integer> entry: projectReg.entrySet()){
                System.out.println("=COUNTIF(\'" + entry.getKey() + "\'!B2:B" + (entry.getValue()+1) + ",\"="+ i + "\")/\'" + entry.getKey() + "\'!H6*100");
            }
            System.out.println();
        }
    }
}
