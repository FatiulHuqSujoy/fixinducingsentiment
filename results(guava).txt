Fix inducing commits: Count = 486 Average Sentiment = -0.002058
Non-fix inducing commits: Count = 5967 Average Sentiment = -0.004525
Neutral fix inducing commits: 420/486(86.42%)
Neutral Non-fix inducing commits: 5519/5967(92.49%)

Fix inducing Sentiment Count: 
	-4: 0 (0.00%)
	-3: 2 (0.41%)
	-2: 5 (1.03%)
	-1: 24 (4.94%)
	0: 420 (86.42%)
	1: 31 (6.38%)
	2: 4 (0.82%)
	3: 0 (0.00%)
	4: 0 (0.00%)

Non-fix inducing Sentiment Count: 
	-4: 0 (0.00%)
	-3: 7 (0.12%)
	-2: 65 (1.09%)
	-1: 143 (2.40%)
	0: 5519 (92.49%)
	1: 203 (3.40%)
	2: 26 (0.44%)
	3: 4 (0.07%)
	4: 0 (0.00%)

Sentiment and commit type relation:
	-4: fix [0, 0.00%],     non-fix [0, 0.00%]
	-3: fix [2, 22.22%],    non-fix [7, 77.78%]
	-2: fix [5, 7.14%],     non-fix [65, 92.86%]
	-1: fix [24, 14.37%],   non-fix [143, 85.63%]
	0:  fix [420, 7.07%],   non-fix [5519, 92.93%]
	1:  fix [31, 13.25%],   non-fix [203, 86.75%]
	2:  fix [4, 13.33%],    non-fix [26, 86.67%]
	3:  fix [0, 0.00%],     non-fix [4, 100.00%]
	4:  fix [0, 0.00%],     non-fix [0, 0.00%]