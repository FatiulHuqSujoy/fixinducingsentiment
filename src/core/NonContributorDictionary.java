package core;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class NonContributorDictionary {
    private Map<String, List<String>> nonContributors = new TreeMap<>();

    public NonContributorDictionary(){
        nonContributors.put("tomcat", getTomcatList());
        nonContributors.put("guava", getGuavaList());
        nonContributors.put("mockito", getMockitoList());
        nonContributors.put("commons-lang", getCommonsLangList());
    }

    public List<String> getNonContributors (String projectName){
        return nonContributors.get(projectName);
    }

    private List<String> getTomcatList() {
        List<String> list = new ArrayList<>();
        list.add("(no author1)");
        return list;
    }

    private List<String> getGuavaList() {
        List<String> list = new ArrayList<>();
        list.add("(no author)");
        list.add("=?UTF-8?q?Grzegorz=20Ro=C5=BCniecki?=");
        list.add("components-build");
        list.add("components-release");
        list.add("guava.mirrorbot@gmail.com");
        return list;
    }

    private List<String> getMockitoList() {
        List<String> list = new ArrayList<>();
        list.add("(no author1)");
        return list;
    }

    private List<String> getCommonsLangList() {
        List<String> list = new ArrayList<>();
        list.add("(no author1)");
        return list;
    }
}
