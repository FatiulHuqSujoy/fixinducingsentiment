package core;

import uk.ac.wlv.sentistrength.SentiStrength;

import java.io.File;

public class SentimentCalculator {
    private String dictionaryLocation = new File("").getAbsolutePath()+"\\senti_dictionary\\";
    private final SentiStrength sentiStrength;

    public SentimentCalculator(){
        sentiStrength = new SentiStrength();
        String ssthInitialisation[] = {"sentidata", dictionaryLocation, "explain"};
        sentiStrength.initialise(ssthInitialisation);
    }

    public int calculateSentiment(String text){
        String result = getSentimentResult(text);
        //System.out.println(result);

        int finalScore = getScoreFromResult(result);
        return finalScore;
    }

    public String getSentimentResult(String text) {
        String result = sentiStrength.computeSentimentScores(text);
        return result;
    }

    public int getScoreFromResult(String result) {
        String[] results = result.split(" ");
        int positiveScore = Integer.parseInt(results[0]);
        int negativeScore = Integer.parseInt(results[1]);

        return getFinalScore(positiveScore, negativeScore);
    }

    private int getFinalScore(int positiveScore, int negativeScore) {
        return positiveScore+negativeScore;
    }
}
