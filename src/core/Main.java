package core;

import org.eclipse.jgit.api.errors.GitAPIException;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        AnalysisInitiator analysisInitiator = new AnalysisInitiator();
        try {
            analysisInitiator.initiateAnalysis();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (GitAPIException e) {
            e.printStackTrace();
        }
    }
}
