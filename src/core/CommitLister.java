package core;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class CommitLister {
    private static final String FIX_INDUCING_DIR = "fix_inducing_commits/";
    private static final String ALL_DIR = "all_commits/";

    public ArrayList<String> listFIxInducingCommits(String project) throws FileNotFoundException {
        String commitListPath = FIX_INDUCING_DIR + project.split("/")[1] + "_commits.txt";
        return getCommitList(commitListPath);
    }

    public ArrayList<String> listAllCommits(String project) {
        String commitListPath = ALL_DIR + project.split("/")[1] + "_commits.txt";
        try {
            return getCommitList(commitListPath);
        } catch (FileNotFoundException e) {
            return new ArrayList<>();
        }
    }

    private ArrayList<String> getCommitList(String commitListPath) throws FileNotFoundException {
        Scanner s = new Scanner(new File(commitListPath));

        ArrayList<String> commitList = new ArrayList<>();
        while (s.hasNext()) {
            commitList.add(s.next());
        }
        s.close();

        return commitList;
    }
}
