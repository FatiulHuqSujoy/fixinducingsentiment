package core;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.revwalk.RevCommit;

import java.io.*;
import java.util.*;

public class CommitAnalyzer {
    private String ANALYSIS_PROJECTS_DIR = "analysis_projects";
    public static final String RESULT_DIR = "results/";

    private List<RevCommit> revCommits, fixCommits, regularCommits;
    public List<Integer> fixInducingSentiments, regularSentiments;
    private Git git;
    private String projectName;

    private SentimentCalculator sentimentCalculator;

    private ArrayList<String> allCommitShas, fixCommitShas;
    private int totalNonFixSenti, totalFixSenti, nonFixCount, fixCount;
    private int zeroNonFixCount, zeroFixCount;
    private Map<Integer, Integer> fixSentimentCount, nonFixSentimentCount;

    public CommitAnalyzer(String projectPath, ArrayList<String> allCommits) throws IOException, GitAPIException {
        revCommits = new ArrayList<>();
        fixCommits = new ArrayList<>();
        regularCommits = new ArrayList<>();
        fixInducingSentiments = new ArrayList<>();
        regularSentiments = new ArrayList<>();
        allCommitShas = allCommits;
        fixCommitShas = new ArrayList<>();
        fixSentimentCount = new TreeMap<>();
        nonFixSentimentCount = new TreeMap<>();
        for(int i=-4; i<=4; i++){
            fixSentimentCount.put(i, 0);
            nonFixSentimentCount.put(i, 0);
        }
        openRepo(projectPath);
    }

    public void openRepo(String projectPath) throws IOException, GitAPIException {
        projectName = projectPath.split("/")[1];

        String filePath = "../" + ANALYSIS_PROJECTS_DIR + "/" + projectName + "/";
        File index = new File(filePath);
        if (index.exists()) {
            git = Git.open(new File(filePath));
            git.checkout();
        }
        else {
            git = Git.cloneRepository()
                    .setURI( "https://github.com/" + projectPath + ".git" )
                    .setDirectory(new File(filePath))
                    .call();
        }
    }

    public void iterateAndProcessCommits(ArrayList<String> fixCommits) throws GitAPIException, IOException {
        sentimentCalculator = new SentimentCalculator();
        fixCommitShas = fixCommits;

        int count = 0, allCount = 0;
        Iterable<RevCommit> revCommits = git.log().all().call();


        System.out.print("\nITERATING COMMITS: ");
        for (RevCommit revCommit : revCommits) {
            if(commitIsApplicable(revCommit.getName())){
                System.out.print(count+1 + ", ");
                analyzeRevCommit(revCommit);
                count++;
            }
            allCount++;
        }
        System.out.println("\ncount: " + count + " " + allCount);
    }

    private boolean commitIsApplicable(String id) {
        if(allCommitShas.size()==0)
            return true;
        if(allCommitShas.contains(id))
            return true;
        return false;
    }

    private void analyzeRevCommit(RevCommit commit){
        String message = commit.getFullMessage();
        int sentiment = sentimentCalculator.calculateSentiment(message);

        if(fixCommitShas.contains(commit.getName())){
            totalFixSenti += sentiment;
            fixCount ++;
            if(sentiment == 0) zeroFixCount++;
            fixCommits.add(commit);

            int sentimentFreq = fixSentimentCount.get(sentiment) + 1;
            fixSentimentCount.put(sentiment, sentimentFreq);

            fixInducingSentiments.add(sentiment);
        }
        else{
            totalNonFixSenti += sentiment;
            nonFixCount ++;
            if(sentiment == 0) zeroNonFixCount++;
            regularCommits.add(commit);

            int sentimentFreq = nonFixSentimentCount.get(sentiment) + 1;
            nonFixSentimentCount.put(sentiment, sentimentFreq);

            regularSentiments.add(sentiment);
        }
    }

    public void printResults() throws FileNotFoundException {
        System.out.println("Fix inducing commits:" +
                " Count = " + fixCount +
                " Average Sentiment = " + getStringFormat((double) totalFixSenti/fixCount, 6));
        System.out.println("Non-fix inducing commits:" +
                " Count = " + nonFixCount +
                " Average Sentiment = " + getStringFormat((double) totalNonFixSenti/nonFixCount, 6));
        System.out.println("Neutral fix inducing commits: " + zeroFixCount + "/" + fixCount +
                "(" + getStringPercentage(zeroFixCount, fixCount) + ")");
        System.out.println("Neutral Non-fix inducing commits: " + zeroNonFixCount + "/" + nonFixCount +
                "(" + getStringPercentage(zeroNonFixCount, nonFixCount) + ")");

        System.out.println("\nFix inducing Sentiment Count: ");
        for(Map.Entry<Integer, Integer> freq: fixSentimentCount.entrySet()){
            System.out.println("\t" + freq.getKey() + ": " + freq.getValue() +
                    " (" + getStringPercentage(freq.getValue(), fixCount) + ")");
        }

        System.out.println("\nNon-fix inducing Sentiment Count: ");
        for(Map.Entry<Integer, Integer> freq: nonFixSentimentCount.entrySet()){
            System.out.println("\t" + freq.getKey() + ": " + freq.getValue() +
                    " (" + getStringPercentage(freq.getValue(), nonFixCount) + ")");
        }

        System.out.println("\nSentiment and commit type relation: ");
        for(int i=-4; i<=4; i++){
            int fix = fixSentimentCount.get(i);
            int nonFix = nonFixSentimentCount.get(i);
            int total = fix+nonFix;
            if(total==0) total=1;

            System.out.println("\t" + i + ": " +
                            "fix [" + fix + ", " + getStringPercentage(fix, total) + "], " +
                            "non-fix [" + nonFix + ", " + getStringPercentage(nonFix, total) + "]"
                    );
        }

//        writeFixCommitDetailsToFile();
        writeRegularCommitDetailsToFile();
//        writeFixCommitListToFile();
//        writeRegularCommitListToFile();
    }

    private void writeFixCommitDetailsToFile() {
        BufferedWriter writer;
        String folderName = RESULT_DIR + projectName;
        new File(folderName).mkdir();
        String fileName = folderName + "/fix_inducing_commits_sentiment.txt";
        try {
            writer = new BufferedWriter(new FileWriter(fileName, false));
            for(RevCommit commit: fixCommits){
                String msg = "---------------------------" +
                        commit.name() + "\n" +
                        "Score: " + sentimentCalculator.calculateSentiment(commit.getFullMessage()) + "\n" +
                        sentimentCalculator.getSentimentResult(commit.getFullMessage()) + "\n\n";
                writer.append(msg);
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeRegularCommitDetailsToFile() {
        BufferedWriter writer;
        String folderName = RESULT_DIR + projectName;
        new File(folderName).mkdir();
        String fileName = folderName + "/regular_commits_sentiment.txt";
        try {
            writer = new BufferedWriter(new FileWriter(fileName, false));
            for(RevCommit commit: regularCommits){
                String msg = "---------------------------" +
                        commit.name() + "\n" +
                        "Score: " + sentimentCalculator.calculateSentiment(commit.getFullMessage()) + "\n" +
                        sentimentCalculator.getSentimentResult(commit.getFullMessage()) + "\n\n";
                writer.append(msg);
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeFixCommitListToFile() throws FileNotFoundException {
        String folderName = RESULT_DIR + projectName;
        new File(folderName).mkdir();

        String fileName = folderName + "/fix_inducing_sentiment.csv";
        PrintWriter csvContentWriter = new PrintWriter(fileName);

        for(Integer sentiment: fixInducingSentiments){
            csvContentWriter.println(sentiment);
        }
        csvContentWriter.close();

        System.out.println("size 1: " + fixInducingSentiments.size());
    }

    private void writeRegularCommitListToFile() throws FileNotFoundException {
        String folderName = RESULT_DIR + projectName;
        new File(folderName).mkdir();

        String fileName = folderName + "/regular_sentiment.csv";
        PrintWriter csvContentWriter = new PrintWriter(fileName);

        for(Integer sentiment: regularSentiments){
            csvContentWriter.println(sentiment);
        }
        csvContentWriter.close();

        System.out.println("size 2: " + regularSentiments.size());
    }

    private String getStringPercentage(int x, int y) {
        double division = (double) x*100/y;
        String result = getStringFormat(division, 2) + "%";
        return result;
    }

    private String getStringFormat(double division, int decimal) {
        String format = "%." + decimal + "f";
        return String.format(format, division);
    }
}
