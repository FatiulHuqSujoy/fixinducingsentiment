package core;

import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.apache.commons.math3.stat.inference.TTest;

import java.util.ArrayList;
import java.util.List;

public class Contributor {
    private String name;
    private List<Integer> fixInducingSentiments, regularSentiments;

    public Contributor (){
        fixInducingSentiments = new ArrayList<>();
        regularSentiments = new ArrayList<>();
    }

    public Contributor (String name1){
        this();
        name = name1;
    }

    public void addFixInducingSentiment(int x){
        fixInducingSentiments.add(x);
    }

    public void addRegularSentiment(int x){
        regularSentiments.add(x);
    }

    public int getTotalCommitNumber(){
        return fixInducingSentiments.size() + regularSentiments.size();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getFixInducingSentiments() {
        return fixInducingSentiments;
    }

    public void setFixInducingSentiments(List<Integer> fixInducingSentiments) {
        this.fixInducingSentiments = fixInducingSentiments;
    }

    public List<Integer> getRegularSentiments() {
        return regularSentiments;
    }

    public void setRegularSentiments(List<Integer> regularSentiments) {
        this.regularSentiments = regularSentiments;
    }

    public Integer getFixInducingSentiment(int i) {
        return fixInducingSentiments.get(i);
    }

    public Integer getRegularSentiment(int i) {
        return regularSentiments.get(i);
    }

    public boolean hasMoreFixInducing() {
        return fixInducingSentiments.size()>regularSentiments.size();
    }

    public String getCommaSepStats(){
        double[] fixInducingSentimentDoubles = getDoubleArray(fixInducingSentiments);
        double[] regularSentimentDoubles = getDoubleArray(regularSentiments);
        double pValue = getPValue(fixInducingSentimentDoubles, regularSentimentDoubles);
        Boolean effectExists = pValue<.05;
        double hedgesG = getHedgesG(fixInducingSentimentDoubles, regularSentimentDoubles);

        String output = name;
        output += "," + getTotalCommitNumber();
        output += "," + fixInducingSentiments.size();
        output += "," + regularSentiments.size();
        output += "," + formatDouble(getMean(fixInducingSentimentDoubles));
        output += "," + formatDouble(getMean(regularSentimentDoubles));
        output += "," + formatDouble(getStandardDeviation(fixInducingSentimentDoubles));
        output += "," + formatDouble(getStandardDeviation(regularSentimentDoubles));
        output += "," + formatDouble(pValue);
        output += "," + effectExists;
        output += "," + formatDouble(hedgesG);
        output += "," + getSignificanceLevel(effectExists, hedgesG);
        output += "," + formatDouble(percNeutral(fixInducingSentiments));
        output += "," + formatDouble(percNeutral(regularSentiments));
        output += "," + formatDouble(percNegative(fixInducingSentiments));
        output += "," + formatDouble(percNegative(regularSentiments));
        output += "," + formatDouble(percPositive(fixInducingSentiments));
        output += "," + formatDouble(percPositive(regularSentiments));

        return output;
    }

    private String formatDouble(double d){
        return String.format("%.2f", d);
    }

    private double percNeutral(List<Integer> sentimentList) {
        int zeroCount = 0;
        for(int sentiment : sentimentList){
            if(sentiment==0)
                zeroCount++;
        }
        return (double) zeroCount/sentimentList.size()*100;
    }

    private double percNegative(List<Integer> sentimentList) {
        int negCount = 0;
        for(int sentiment : sentimentList){
            if(sentiment<0)
                negCount++;
        }
        return (double) negCount/sentimentList.size()*100;
    }

    private double percPositive(List<Integer> sentimentList) {
        int posCount = 0;
        for(int sentiment : sentimentList){
            if(sentiment>0)
                posCount++;
        }
        return (double) posCount/sentimentList.size()*100;
    }

    private String getSignificanceLevel(Boolean effectExists, double hedgesG) {
        if(!effectExists)
            return "N/A";
        if(hedgesG<.2)
            return "Trivial";
        if(hedgesG<.5)
            return "Small";
        if(hedgesG<.8)
            return "Medium";
        return "Large";
    }

    private double getHedgesG(double[] sample1, double[] sample2) {
        double meanDifference = Math.abs(getMean(sample1) - getMean(sample2));
        double sdPooledX = getSDPooledX(sample1, sample2);

        return meanDifference/sdPooledX;
    }

    private double getSDPooledX(double[] sample1, double[] sample2) {
        double sampleSD1 = (sample1.length-1) * getStandardDeviation(sample1);
        double sampleSD2 = (sample2.length-1) * getStandardDeviation(sample2);
        return Math.sqrt((sampleSD1+sampleSD2)/(sample1.length+sample2.length-2));
    }

    private double getPValue(double[] sample1, double[] sample2) {
        double meanDifference = Math.abs(getMean(sample1) - getMean(sample2));
        double variance1 = getStandardDeviation(sample1);
        double variance2 = getStandardDeviation(sample2);

        return meanDifference /
                Math.sqrt(
                        variance1/(sample1.length-1) +
                        variance2/(sample2.length-1)
                );

//        TTest tTest = new TTest();
//        return tTest.homoscedasticTTest(sample1, sample2);
    }

    private double getStandardDeviation(double[] sample) {
        StandardDeviation sd = new StandardDeviation();
        return sd.evaluate(sample);
    }

    private double getMean(double[] sample) {
        Mean mean = new Mean();
        return mean.evaluate(sample);
    }

    public double[] getDoubleArray(List<Integer> integerList) {
        double[] doubleArray = new double[integerList.size()];
        for(int i=0; i<integerList.size(); i++){
            doubleArray[i] = integerList.get(i);
        }
        return doubleArray;
    }

    public boolean isNotWorthy() {
        return getTotalCommitNumber()<10 || fixInducingSentiments.size()<2 || regularSentiments.size()<2;
    }
}
