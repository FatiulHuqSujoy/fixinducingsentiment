package core;

import org.eclipse.jgit.api.errors.GitAPIException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class AnalysisInitiator {
    private static List<Integer> fixes, regulars;

    public AnalysisInitiator() {
        fixes = new ArrayList<>();
        regulars = new ArrayList<>();
    }

    public void initiateAnalysis() throws IOException, GitAPIException {
        for(String project : ProjectLister.listProjects()){
            System.out.println("\nANALYZING PROJECT: " + project);
            analyzeAllCommits(project);
//            analyzeContributorCommits(project);
        }
//        printAllCommits();
    }

    private void analyzeContributorCommits(String project) {
        try {
            CommitLister commitLister = new CommitLister();
            ContributorCommitAnalyzer contributorCommitAnalyzer = new ContributorCommitAnalyzer(project,
                    commitLister.listAllCommits(project));
            contributorCommitAnalyzer.iterateAndProcessCommits(commitLister.listFIxInducingCommits(project));
            contributorCommitAnalyzer.printResults();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (GitAPIException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void analyzeAllCommits(String project) {
        try {
            CommitLister commitLister = new CommitLister();
            CommitAnalyzer commitAnalyzer = new CommitAnalyzer(project,
                    commitLister.listAllCommits(project));
            commitAnalyzer.iterateAndProcessCommits(commitLister.listFIxInducingCommits(project));
            commitAnalyzer.printResults();

            fixes.addAll(commitAnalyzer.fixInducingSentiments);
            regulars.addAll(commitAnalyzer.regularSentiments);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (GitAPIException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printAllCommits() throws FileNotFoundException {
        String folderName = "results/all projects";
        new File(folderName).mkdir();

        String fileName = folderName + "/regular_sentiment.csv";
        PrintWriter csvContentWriter = new PrintWriter(fileName);

        for(Integer sentiment: regulars){
            csvContentWriter.println(sentiment);
        }

        fileName = folderName + "/fix_inducing_sentiment.csv";
        csvContentWriter = new PrintWriter(fileName);

        for(Integer sentiment: fixes){
            csvContentWriter.println(sentiment);
        }
        csvContentWriter.close();
    }
}
