package core;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.revwalk.RevCommit;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class ContributorCommitAnalyzer {
    private String ANALYSIS_PROJECTS_DIR = "analysis_projects";
    public static final String RESULT_DIR = "contributor_results/";

    private List<Integer> fixInducingSentiments, regularSentiments;
    private Map<String, Contributor> contributors;
    private List<String> nonContributors;

    private Git git;
    private String projectName;

    private SentimentCalculator sentimentCalculator;

    private ArrayList<String> allCommitShas, fixInducingCommitShas;

    public ContributorCommitAnalyzer(String projectPath, ArrayList<String> allCommits) throws IOException, GitAPIException {
        fixInducingSentiments = new ArrayList<>();
        regularSentiments = new ArrayList<>();
        contributors = new TreeMap<>();
        allCommitShas = allCommits;
        fixInducingCommitShas = new ArrayList<>();

        openRepo(projectPath);
        populateNonContributors(projectName);
    }

    public void openRepo(String projectPath) throws IOException, GitAPIException {
        projectName = projectPath.split("/")[1];

        String filePath = "../" + ANALYSIS_PROJECTS_DIR + "/" + projectName + "/";
        File index = new File(filePath);
        if (index.exists()) {
            git = Git.open(new File(filePath));
            git.checkout();
        }
        else {
            git = Git.cloneRepository()
                    .setURI( "https://github.com/" + projectPath + ".git" )
                    .setDirectory(new File(filePath))
                    .call();
        }
    }

    private void populateNonContributors(String projectName) {
        NonContributorDictionary nonContributorDictionary = new NonContributorDictionary();
        nonContributors = nonContributorDictionary.getNonContributors(projectName);
    }

    public void iterateAndProcessCommits(ArrayList<String> fixCommits) throws GitAPIException, IOException {
        sentimentCalculator = new SentimentCalculator();
        fixInducingCommitShas = fixCommits;

        int count = 0;
        Iterable<RevCommit> revCommits = git.log().all().call();

        System.out.print("\nITERATING COMMITS: ");
        for (RevCommit revCommit : revCommits) {
            if(commitIsApplicable(revCommit.getName())){
                System.out.print(count+1 + ", ");
                analyzeRevCommit(revCommit);
                count++;
            }
        }
        System.out.println("\ncount: " + count);
    }

    private boolean commitIsApplicable(String id) {
        if(allCommitShas.size()==0)
            return true;
        if(allCommitShas.contains(id))
            return true;
        return false;
    }

    private void analyzeRevCommit(RevCommit commit){
        String message = commit.getFullMessage();
        int sentiment = sentimentCalculator.calculateSentiment(message);

        String contributorName = commit.getAuthorIdent().getName();
        if(nonContributors.contains(contributorName))
            return;

        Contributor contributor = getContributorFromIndex(contributorName);

        if(fixInducingCommitShas.contains(commit.getName())){
            contributor.addFixInducingSentiment(sentiment);
            fixInducingSentiments.add(sentiment);
        }
        else{
            contributor.addRegularSentiment(sentiment);
            regularSentiments.add(sentiment);
        }

        contributors.put(contributorName, contributor);
    }

    private Contributor getContributorFromIndex(String name) {
        if(!contributors.containsKey(name))
            return new Contributor(name);
        else
            return contributors.get(name);
    }

    public void printResults() throws FileNotFoundException {
        System.out.print("PRINTING RESULTS: ");
        String output = getHeadings();

        for(Map.Entry<String, Contributor> entry : contributors.entrySet()){
            Contributor contributor = entry.getValue();
            if(contributor.isNotWorthy())
                continue;
            System.out.print(contributor.getName() + ", ");

            output += "\n" + contributor.getCommaSepStats();
            writeIndividualContributorResult(contributor);
        }

        writeStringToCSV(output, "contributor_stats");
    }

    private String getHeadings() {
        return "core.Contributor Name, Total Commits, F-Count, R-Count," +
                "F-Mean, R-Mean," +
                "F-SD, R-SD," +
                "P, Effect Exists, Hedges' G, Significance," +
                "F-Neutral %, R-Neutral %," +
                "F-Negative %, R-Negative %," +
                "F-Positive %, R-Positive %,";
    }

    private void writeIndividualContributorResult(Contributor contributor) throws FileNotFoundException {
        String output = "Fix-Inducing, Regular";

        if(contributor.hasMoreFixInducing()){
            int minSize = contributor.getRegularSentiments().size();
            int maxSize = contributor.getFixInducingSentiments().size();
            for(int i=0; i<minSize; i++){
                output += "\n" + contributor.getFixInducingSentiment(i) + "," + contributor.getRegularSentiment(i);
            }
            for(int i=minSize; i<maxSize; i++){
                output += "\n" + contributor.getFixInducingSentiment(i) + ",";
            }
        }
        else{
            int minSize = contributor.getFixInducingSentiments().size();
            int maxSize = contributor.getRegularSentiments().size();
            for(int i=0; i<minSize; i++){
                output += "\n" + contributor.getFixInducingSentiment(i) + "," + contributor.getRegularSentiment(i);
            }
            for(int i=minSize; i<maxSize; i++){
                output += "\n," + contributor.getRegularSentiment(i);
            }
        }

        writeStringToCSV(output, contributor.getName()+"_sentiment");
    }

    private void writeStringToCSV(String output, String fileName) throws FileNotFoundException {
        String folderName = RESULT_DIR + projectName;
        new File(folderName).mkdir();

        String filePath = folderName + "/" + fileName + ".csv";
        PrintWriter csvContentWriter = new PrintWriter(filePath);
        csvContentWriter.print(output);

        csvContentWriter.close();
    }
}
