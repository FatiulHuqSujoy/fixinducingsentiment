package extra;

import core.SentimentCalculator;

public class SentimentSeResult {
    public static void main(String[] args) {
        String text = "HDDS-938. Add Client APIs for using S3 Auth interface.\n" +
                "Contributed by  Dinesh Chitlangia.";

        SentimentCalculator sentimentCalculator = new SentimentCalculator();
        System.out.println(sentimentCalculator.getSentimentResult(text));
    }
}
